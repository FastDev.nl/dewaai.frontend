<?php

use Illuminate\Database\Seeder;
use App\Models\Instructor;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $instructor = new Instructor();
        $instructor->email = "instructor@gmail.com";
        $instructor->password = bcrypt('secret1234');
        $instructor->name_first = "Test";
        $instructor->name_last = "Instructor";
        $instructor->active = 1;
        $instructor->address_street = "na";
        $instructor->address_number = "na";
        $instructor->address_postcode = "na";
        $instructor->address_city = "na";
        $instructor->iban = "na";
        $instructor->birthdate = \Carbon\Carbon::now();
        $instructor->phonenumber = "0123456789";
        $instructor->country = "Nederland";
        $instructor->save();

        $learner = new \App\Models\Learner();
        $learner->email = "learner@gmail.com";
        $learner->password = bcrypt('secret1234');
        $learner->name_first = "Test";
        $learner->name_last = "Learner";
        $learner->address_street = "na";
        $learner->address_number = "na";
        $learner->address_postcode = "na";
        $learner->address_city = "na";
        $learner->birthdate = \Carbon\Carbon::now();
        $learner->confirmed = 1;
        $learner->phonenumber = "0123456789";
        $learner->country = "Nederland";
        $learner->save();
    }
}
