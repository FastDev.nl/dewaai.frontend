<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesInstructorsAssignmentsTable extends Migration {

	public function up()
	{
		Schema::create('courses_instructors_assignments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('course_id')->unsigned();
			$table->integer('instructor_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('courses_instructors_assignments');
	}
}