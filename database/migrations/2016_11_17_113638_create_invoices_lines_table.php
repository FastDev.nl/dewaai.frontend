<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesLinesTable extends Migration {

	public function up()
	{
		Schema::create('invoices_lines', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('invoice_id')->unsigned()->index();
			$table->string('text', 255);
			$table->integer('amount')->default('1');
			$table->decimal('price');
		});
	}

	public function down()
	{
		Schema::drop('invoices_lines');
	}
}