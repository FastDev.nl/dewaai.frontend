<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instructors', function (Blueprint $table) {
            $table->string('country', 255)->default('Nederland');
        });
        Schema::table('learners', function (Blueprint $table) {
            $table->string('country', 255)->default('Nederland');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instructors', function (Blueprint $table) {
            $table->dropColumn('country');
        });
        Schema::table('learners', function (Blueprint $table) {
            $table->dropColumn('country');
        });
    }
}
