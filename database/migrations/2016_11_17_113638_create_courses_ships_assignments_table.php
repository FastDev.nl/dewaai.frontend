<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesShipsAssignmentsTable extends Migration {

	public function up()
	{
		Schema::create('courses_ships_assignments', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('course_id')->unsigned();
			$table->integer('ship_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('courses_ships_assignments');
	}
}