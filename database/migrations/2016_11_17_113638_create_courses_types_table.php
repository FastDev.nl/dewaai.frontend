<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTypesTable extends Migration {

	public function up()
	{
		Schema::create('courses_types', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name', 255)->index();
			$table->boolean('active')->default(0);
			$table->integer('ship_type_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('courses_types');
	}
}