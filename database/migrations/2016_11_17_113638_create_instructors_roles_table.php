<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstructorsRolesTable extends Migration {

	public function up()
	{
		Schema::create('instructors_roles', function(Blueprint $table) {
			$table->increments('id');
			$table->softDeletes();
			$table->string('title', 255)->index();
			$table->boolean('access_management')->default(0);
			$table->boolean('access_weblogin')->default(1);
		});
	}

	public function down()
	{
		Schema::drop('instructors_roles');
	}
}