<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipsTable extends Migration {

	public function up()
	{
		Schema::create('ships', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('name', 255)->index();
			$table->integer('type_id')->unsigned();
			$table->text('description')->nullable();
			$table->boolean('active')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('ships');
	}
}