<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoursesTypesReaddShipType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses_types', function (Blueprint $table) {
            Schema::table('courses_types', function (Blueprint $table) {
                $table->integer('ship_type')->unsigned()->nullable();
                $table->foreign('ship_type')->references('id')->on('ships_types');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses_types', function (Blueprint $table) {
            Schema::table('courses_types', function (Blueprint $table) {
                $table->dropForeign('courses_types_ship_type_foreign');
                $table->dropColumn('ship_type');
            });
        });
    }
}
