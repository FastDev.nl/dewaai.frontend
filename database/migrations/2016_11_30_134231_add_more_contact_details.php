<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreContactDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instructors', function (Blueprint $table) {
            $table->string('address_street', 255)->default('na');
            $table->string('address_number', 30)->default('na');
            $table->string('address_postcode', 10)->default('na');
            $table->string('address_city', 255)->default('na');
            $table->string('iban', 20)->default('na');
            $table->date('birthdate')->default(date('Y-m-d H:i:s'));
            $table->string('phonenumber')->default('0');
        });
        Schema::table('learners', function (Blueprint $table) {
            $table->string('phonenumber')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instructors', function (Blueprint $table) {
            //
        });
    }
}
