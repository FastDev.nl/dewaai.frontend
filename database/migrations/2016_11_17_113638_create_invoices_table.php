<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	public function up()
	{
		Schema::create('invoices', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('learner_id')->unsigned();
			$table->integer('course_id')->unsigned()->nullable();
			$table->integer('status')->unsigned()->default('1');
		});
	}

	public function down()
	{
		Schema::drop('invoices');
	}
}