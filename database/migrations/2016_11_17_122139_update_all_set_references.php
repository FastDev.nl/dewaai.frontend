<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAllSetReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses_instructors_assignments', function(Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('instructor_id')->references('id')->on('instructors');
        });
        Schema::table('courses_learners_assignments', function(Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('learner_id')->references('id')->on('learners');
        });
        Schema::table('courses_ships_assignments', function(Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('ship_id')->references('id')->on('ships');
        });
        Schema::table('courses', function(Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('courses_types');
        });
        Schema::table('courses_types', function(Blueprint $table) {
            $table->foreign('ship_type_id')->references('id')->on('ships_types');
        });
        Schema::table('instructors_roles_assignments', function(Blueprint $table) {
            $table->foreign('instructor_id')->references('id')->on('instructors');
            $table->foreign('instructor_role_id')->references('id')->on('instructors_roles');
        });
        Schema::table('invoices_lines', function(Blueprint $table) {
            $table->foreign('invoice_id')->references('id')->on('invoices');
        });
        Schema::table('invoices', function(Blueprint $table) {
            $table->foreign('course_id')->references('id')->on('courses');
        });
        Schema::table('ships', function(Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('ships_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
