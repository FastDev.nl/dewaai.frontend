<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssignmentsAcceptedToStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses_learners_assignments', function (Blueprint $table) {
            $table->dropColumn('accepted');
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses_learners_assignments', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->boolean('accepted')->default(0);
        });
    }
}
