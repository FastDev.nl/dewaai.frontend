<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLearnersTable extends Migration {

	public function up()
	{
		Schema::create('learners', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('email', 255)->unique();
			$table->string('password', 255);
			$table->string('name_first', 255);
			$table->string('name_last', 255);
			$table->string('address_street', 255);
			$table->string('address_number', 30);
			$table->string('address_postcode', 10);
			$table->string('address_city', 255);
			$table->date('birthdate');
		});
	}

	public function down()
	{
		Schema::drop('learners');
	}
}