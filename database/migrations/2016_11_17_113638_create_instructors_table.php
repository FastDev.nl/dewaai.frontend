<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstructorsTable extends Migration {

	public function up()
	{
		Schema::create('instructors', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('email', 255)->unique();
			$table->string('password', 255);
			$table->string('name_first', 255);
			$table->string('name_last', 255);
			$table->boolean('can_assigned')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('instructors');
	}
}