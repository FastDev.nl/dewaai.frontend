<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstructorsRolesAssignmentsTable extends Migration {

	public function up()
	{
		Schema::create('instructors_roles_assignments', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('instructor_id')->unsigned();
			$table->integer('instructor_role_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('instructors_roles_assignments');
	}
}