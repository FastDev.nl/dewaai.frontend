<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('pages.home');
});
Route::get('information', function () {
    return view('pages.information');
});
Route::get('contact', function () {
    return view('pages.contact');
});
Route::post('contact', 'HomepageController@postContact');

Route::group([
    'prefix' => 'signup',
    'namespace' => 'Learner'
], function () {
    Route::get('/', 'SignupController@getList');
    Route::post('/', 'SignupController@postList');

    Route::get('auth', 'SignupController@getAuth');
    Route::post('auth', 'SignupController@postAuth');

    Route::get('form', function () {
        $month = date('m');
        return redirect('signup/form/'.$month);
    });
    Route::get('form/{month}', 'SignupController@getForm');
    Route::post('form', 'SignupController@postForm');

    Route::get('month/{month}', 'SignupController@getMonth');

    Route::get('complete', 'SignupController@getComplete');
});

Route::group([
    'prefix' => 'learner',
    'middleware' => 'auth:learner',
    'namespace' => 'Learner'
], function () {
    Route::get('/', 'OverviewController@getIndex');
    Route::get('calendar', 'OverviewController@getCalendar');
    Route::get('invoices', 'OverviewController@getInvoices');

    Route::get('details', 'OverviewController@getDetails');
    Route::post('details', 'OverviewController@postDetails');
});

Route::get('invoice/{invoice_code}/{invoice_hash}', 'InvoiceController@getInvoice');

Route::group([
    'prefix' => 'instructor',
    'middleware' => 'auth:instructor',
    'namespace' => 'Instructor'
], function () {
    Route::get('/', 'OverviewController@getIndex');
    Route::get('calendar', 'OverviewController@getCalendar');
    Route::get('course/{course}', 'OverviewController@getCourse');
    Route::get('details', 'OverviewController@getDetails');
    Route::post('details', 'OverviewController@postDetails');
});

Route::get('login', function () {
    return redirect('auth/login');
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function () {
    Route::get('login', 'LoginController@getLogin');
    Route::get('logout', 'LoginController@getLogout');
    Route::post('login', 'LoginController@postLogin');
});

Auth::routes();
