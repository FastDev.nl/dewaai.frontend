<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomepageTest extends TestCase
{
    public function testSeeTitle()
    {
        $this->visit('/')
            ->see('Zeilschool De Waai');
    }

    public function testCorrectStyleUrl() {
        $this->visit('/')
            ->see('http://localhost/css/style.css');
    }
}
