<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    public function testLoginPage()
    {
        $this->visit('/')
            ->click('Inloggen')
            ->seePageIs('auth/login');
    }

    public function testLearnerLoginForm()
    {
        $this->visit('auth/login')
            ->type('learner@gmail.com', 'email')
            ->type('secret1234', 'password')
            ->press('Inloggen')
            ->seePageIs('learner/calendar')
            ->see('Test Learner')
            ->visit('/')
            ->see('Mijn Account')
            ->visit('auth/logout')
            ->see('U bent nu uitgelogd.');
    }

    public function testInstructorLoginForm()
    {
        $this->visit('auth/login')
            ->type('instructor@gmail.com', 'email')
            ->type('secret1234', 'password')
            ->press('Inloggen')
            ->seePageIs('instructor/calendar')
            ->see('Test Instructor')
            ->visit('/')
            ->see('Mijn Account')
            ->visit('auth/logout')
            ->see('U bent nu uitgelogd.');
    }

    public function testLoginFailureMessage() {
        $this->visit('auth/login')
            ->type('haha@lol.nl', 'email')
            ->type('jemoeder', 'password')
            ->press('Inloggen')
            ->seePageIs('auth/login')
            ->see('De login gegevens zijn incorrect.');
    }
}
