# dewaai.frontend

## installation

```
$ composer install
$ cp .env.example .env
$ php artisan key:generate
```

Set variables in the .env file

```
$ php artisan migrate
$ php artisan make:superuser
```

Fill in details.

Done.