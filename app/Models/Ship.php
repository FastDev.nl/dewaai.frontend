<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Ship
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $name
 * @property integer $type_id
 * @property string $description
 * @property boolean $active
 * @property integer $max_person
 * @property-read \App\Models\ShipType $type
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ship whereMaxPerson($value)
 * @mixin \Eloquent
 */
class Ship extends Model
{

    protected $table = 'ships';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'type_id', 'description', 'active'];

    public function type()
    {
        return $this->hasOne('App\Models\ShipType', 'id', 'type_id');
    }
}
