<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InstructorRoleAssignment
 *
 * @property integer $id
 * @property integer $instructor_id
 * @property integer $instructor_role_id
 * @property-read \App\Models\InstructorRole $role
 * @property-read \App\Models\Instructor $instructor
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRoleAssignment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRoleAssignment whereInstructorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRoleAssignment whereInstructorRoleId($value)
 * @mixin \Eloquent
 */
class InstructorRoleAssignment extends Model
{

    protected $table = 'instructors_roles_assignments';
    public $timestamps = false;
    protected $fillable = ['instructor_id', 'instructor_role_id'];

    public function role()
    {
        return $this->hasOne('App\Models\InstructorRole');
    }

    public function instructor()
    {
        return $this->hasOne('App\Models\Instructor');
    }
}
