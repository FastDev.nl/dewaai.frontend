<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Course
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $type_id
 * @property string $name
 * @property string $date_start
 * @property integer $ship_type
 * @property float $custom_price
 * @property-read \App\Models\CourseType $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Learner[] $learners
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Instructor[] $instructors
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ship[] $ships
 * @property-read mixed $places_left
 * @property-read mixed $date_range
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereShipType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Course whereCustomPrice($value)
 * @mixin \Eloquent
 */
class Course extends Model
{

    protected $table = 'courses';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['type_id', 'name'];
    protected $appends = ['places_left', 'date_range', 'price'];

    protected $day_length = 5;

    public function type()
    {
        return $this->hasOne('App\Models\CourseType', 'id', 'type_id');
    }

    public function learners()
    {
        return $this->belongsTomany('App\Models\Learner', 'courses_learners_assignments');
    }

    public function learners_assignments()
    {
        return $this->hasMany('App\Models\CourseLearnerAssignment');
    }

    public function instructors()
    {
        return $this->hasManyThrough('App\Models\Instructor', 'App\Models\CourseInstructorAssignment');
    }

    public function ships()
    {
        return $this->belongsTomany('App\Models\Ship', 'courses_ships_assignments');
    }

    public function getPlacesLeftAttribute()
    {
        $ships = $this->ships;
        $places = 0;
        foreach ($ships as $ship) {
            $places += $ship->max_person;
        }
        $learners = $this->learners;
        $places -= count($learners);

        return $places;
    }

    public function getDateEndAttribute()
    {
        $start = strtotime($this->date_start);
        $end = $start + ($this->day_length * 86400);
        return date('Y-m-d H:i:s', $end);
    }

    public function getDateRangeAttribute()
    {
        $start = strtotime($this->date_start);
        $end = $start + ($this->day_length * 86400);

        $string = date('d-m-Y', $start) . ' t/m ' . date('d-m-Y', $end);
        return $string;
    }

    public function getPriceAttribute()
    {
        if ($this->custom_price != null) {
            return $this->custom_price;
        }
        return $this->type->price;
    }
}
