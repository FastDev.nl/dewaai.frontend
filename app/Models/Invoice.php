<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Invoice
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $learner_id
 * @property integer $course_id
 * @property integer $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceLine[] $lines
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereLearnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereStatus($value)
 * @mixin \Eloquent
 * @property string $file_name
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereFileName($value)
 * @property string $invoice_code
 * @property string $invoice_hash
 * @property-read \App\Models\Learner $learner
 * @property-read mixed $total
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereInvoiceCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Invoice whereInvoiceHash($value)
 */
class Invoice extends Model
{

    private $statuses = [
        0 => 'Openstaand',
        1 => 'Voldaan',
        2 => 'Geannuleerd'
    ];

    protected $table = 'invoices';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['learner_id', 'course_id', 'status'];

    public function getStatusStringAttribute()
    {
        if (isset($this->statuses[$this->status])) {
            return $this->statuses[$this->status];
        }
        return $this->status;
    }

    public function getUrlAttribute()
    {
        return url('invoice/' . $this->invoice_code . '/' . $this->invoice_hash);
    }

    public function lines()
    {
        return $this->hasMany('App\Models\InvoiceLine', 'invoice_id', 'id');
    }

    public function learner()
    {
        return $this->hasOne('App\Models\Learner', 'id', 'learner_id');
    }

    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->lines as $item) {
            $total += $item->price;
        }
        return $total;
    }
}
