<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CourseType
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $name
 * @property boolean $active
 * @property integer $ship_type
 * @property integer $min_age
 * @property boolean $sleep_food_included
 * @property float $price
 * @property-read \App\Models\ShipType $ship
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereShipType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereMinAge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType whereSleepFoodIncluded($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseType wherePrice($value)
 * @mixin \Eloquent
 */
class CourseType extends Model
{

    protected $table = 'courses_types';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'active', 'ship_type_id'];

    public function ship()
    {
        return $this->hasOne('App\Models\ShipType', 'id', 'ship_type');
    }
}
