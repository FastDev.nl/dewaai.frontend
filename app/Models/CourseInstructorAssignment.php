<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseInstructorAssignment
 *
 * @property integer $id
 * @property integer $course_id
 * @property integer $instructor_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseInstructorAssignment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseInstructorAssignment whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseInstructorAssignment whereInstructorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseInstructorAssignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseInstructorAssignment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CourseInstructorAssignment extends Model
{

    protected $table = 'courses_instructors_assignments';
    public $timestamps = true;
    protected $fillable = ['course_id', 'instructor_id'];
}
