<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseShipAssignment
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $course_id
 * @property integer $ship_id
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseShipAssignment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseShipAssignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseShipAssignment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseShipAssignment whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseShipAssignment whereShipId($value)
 * @mixin \Eloquent
 */
class CourseShipAssignment extends Model
{

    protected $table = 'courses_ships_assignments';
    public $timestamps = true;
    protected $fillable = ['course_id', 'ship_id'];
}
