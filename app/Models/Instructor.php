<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\Models\Instructor
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $email
 * @property string $password
 * @property string $name_first
 * @property string $name_last
 * @property boolean $active
 * @property string $address_street
 * @property string $address_number
 * @property string $address_postcode
 * @property string $address_city
 * @property string $iban
 * @property string $birthdate
 * @property string $phonenumber
 * @property string $country
 * @property string $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InstructorRole[] $roles
 * @property-read mixed $name_full
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereNameFirst($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereNameLast($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereAddressStreet($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereAddressNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereAddressPostcode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereAddressCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereIban($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereBirthdate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor wherePhonenumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Instructor whereRememberToken($value)
 * @mixin \Eloquent
 */
class Instructor extends Authenticatable
{

    protected $table = 'instructors';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['email', 'password', 'name_first', 'name_last', 'can_assigned'];
    protected $hidden = ['password'];

    public function roles()
    {
        return $this->hasManyThrough('App\Models\InstructorRole', 'App\Models\InstructorRoleAssignment');
    }

    public function courses() {
        return $this->belongsToMany('App\Models\Course', 'courses_instructors_assignments');
    }

    public function getNameFullAttribute()
    {
        return $this->name_first . ' ' . $this->name_last;
    }
}
