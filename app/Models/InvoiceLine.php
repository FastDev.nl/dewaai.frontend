<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InvoiceLine
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $invoice_id
 * @property string $text
 * @property integer $amount
 * @property float $price
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine whereInvoiceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InvoiceLine wherePrice($value)
 * @mixin \Eloquent
 */
class InvoiceLine extends Model
{

    protected $table = 'invoices_lines';
    public $timestamps = true;
    protected $fillable = ['invoice_id', 'text', 'amount', 'price'];
}
