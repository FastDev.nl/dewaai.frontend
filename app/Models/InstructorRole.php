<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\InstructorRole
 *
 * @property integer $id
 * @property \Carbon\Carbon $deleted_at
 * @property string $title
 * @property boolean $access_management
 * @property boolean $access_weblogin
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRole whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRole whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRole whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRole whereAccessManagement($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\InstructorRole whereAccessWeblogin($value)
 * @mixin \Eloquent
 */
class InstructorRole extends Model
{

    protected $table = 'instructors_roles';
    public $timestamps = false;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['title', 'access_management', 'access_weblogin'];
}
