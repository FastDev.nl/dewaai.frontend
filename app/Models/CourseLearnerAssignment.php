<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CourseLearnerAssignment
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $course_id
 * @property integer $learner_id
 * @property integer $status
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseLearnerAssignment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseLearnerAssignment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseLearnerAssignment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseLearnerAssignment whereCourseId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseLearnerAssignment whereLearnerId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CourseLearnerAssignment whereStatus($value)
 * @mixin \Eloquent
 */
class CourseLearnerAssignment extends Model
{

    protected $table = 'courses_learners_assignments';
    public $timestamps = true;
    protected $fillable = ['course_id', 'learner_id', 'status'];
    protected $statuses = [
        0 => 'In afwachting',
        1 => 'Geaccepteerd',
        2 => 'Afgewezen'
    ];

    public function getStatusStringAttribute()
    {
        if (isset($this->statuses[$this->status])) {
            return $this->statuses[$this->status];
        }
        return $this->status;
    }

    public function learner() {
        return $this->hasOne('App\Models\Learner', 'id', 'learner_id');
    }
}
