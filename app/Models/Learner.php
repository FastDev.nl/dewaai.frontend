<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword;

/**
 * App\Models\Learner
 *
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $email
 * @property string $password
 * @property string $name_first
 * @property string $name_last
 * @property string $address_street
 * @property string $address_number
 * @property string $address_postcode
 * @property string $address_city
 * @property string $birthdate
 * @property boolean $confirmed
 * @property string $phonenumber
 * @property string $country
 * @property string $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoices
 * @property-read mixed $name_full
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereNameFirst($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereNameLast($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereAddressStreet($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereAddressNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereAddressPostcode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereAddressCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereBirthdate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner wherePhonenumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Learner whereRememberToken($value)
 * @mixin \Eloquent
 */
class Learner extends Authenticatable
{

    protected $table = 'learners';
    public $timestamps = true;

    use SoftDeletes;
    use \Illuminate\Notifications\Notifiable;

    protected $fillable = [
        'email', 'password', 'name_first', 'name_last', 'address_street', 'address_number', 'address_postcode',
        'address_city', 'birthdate', 'confirmed'
    ];
    protected $hidden = ['password'];
    protected $dates = ['birthdate', 'deleted_at'];

    public function invoices()
    {
        return $this->hasMany('App\Models\Invoice');
    }

    public function courses()
    {
        return $this->belongsTomany('App\Models\Course', 'courses_learners_assignments');
    }

    public function getNameFullAttribute()
    {
        return $this->name_first . ' ' . $this->name_last;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
