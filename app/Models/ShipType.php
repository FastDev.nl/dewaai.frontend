<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ShipType
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShipType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShipType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShipType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShipType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ShipType whereDeletedAt($value)
 * @mixin \Eloquent
 */
class ShipType extends Model
{

    protected $table = 'ships_types';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
