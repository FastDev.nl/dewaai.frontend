<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Illuminate\Http\Request;
use PDF;

class InvoiceController extends Controller
{
    public function getInvoice($invoice_code, $invoice_hash)
    {
        $invoice = Invoice::whereInvoiceCode($invoice_code)->where('invoice_hash', $invoice_hash)->firstOrFail();
        return view('invoice.pdf', ['invoice' => $invoice]);
    }
}
