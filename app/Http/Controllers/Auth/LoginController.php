<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {

        $learner = Auth::guard('learner')->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);
        if ($learner) {
            \Session::set('user', Auth::guard('learner')->user());
            return redirect('/learner');
        }
        $instructor = Auth::guard('instructor')->attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);
        if ($instructor) {
            \Session::set('user', Auth::guard('instructor')->user());
            return redirect('/instructor');
        }
        return redirect('/auth/login')->with('error', 'De login gegevens zijn incorrect.');
    }

    public function getLogout()
    {
        Auth::guard('learner')->logout();
        Auth::guard('instructor')->logout();
        \Session::clear();
        return redirect('/auth/login')->with('message', 'U bent nu uitgelogd.');
    }
}
