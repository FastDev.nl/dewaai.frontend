<?php

namespace App\Http\Controllers\Learner;

use App\Jobs\LearnerSignupJob;
use App\Models\CourseLearnerAssignment;
use App\Models\CourseType;
use App\Models\Course;
use Auth;
use App\Models\Learner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SignupController extends Controller
{
    public function getList()
    {
        $types = CourseType::where('active', 1)->get();
        $num = 0;
        $table = 0;
        $tables = [];
        foreach ($types as $ty) {
            if($num == 0) {
                $tables[$table] = [
                    'ids' => [],
                    'headers' => [],
                    'ships' => [],
                    'prices' => [],
                    'ages' => [],
                    'sleep_food' => []
                ];
            }
            $tables[$table]['ids'][] = $ty->id;
            $tables[$table]['headers'][] = $ty->name;
            $tables[$table]['ships'][] = $ty->ship->name;
            $tables[$table]['prices'][] = $ty->price;
            if ($ty->min_age == null) {
                $tables[$table]['ages'][] = "geen minimale leeftijd";
            } else {
                $tables[$table]['ages'][] = "vanaf " . $ty->min_age . " jaar";
            }
            if ($ty->sleep_food_included) {
                $tables[$table]['sleep_food'][] = "Ja";
            } else {
                $tables[$table]['sleep_food'][] = "Nee";
            }
            $num++;
            if($num == 4) {
                $table++;
                $num = 0;
            }
        }
        return view('signup.course_list', [
            'tables' => $tables
        ]);
    }

    public function postList(Request $request)
    {
        \Session::set('type', $request->input('course_id'));
        return redirect('signup/auth');
    }

    public function getAuth()
    {
        if (session('user')) {
            return redirect('signup/form');
        }
        $course_type = CourseType::find(session('type'));
        return view('signup.auth', ['type' => $course_type]);
    }

    public function postAuth(Request $request)
    {
        $action = $request->input('action');
        if ($action == 'login') {
            $learner = Auth::guard('learner')->attempt([
                'email' => $request->input('email'),
                'password' => $request->input('password')
            ]);
            if ($learner) {
                \Session::set('user', Auth::guard('learner')->user());
                return redirect('signup/form');
            }
            return redirect('signup/auth')->with('error', 'Deze inloggegevens zijn incorrect');
        } elseif ($action == 'register') {
            $checks = [
                "name_first" => "required",
                "name_last" => "required",
                "email" => "required|email",
                "password" => "required|confirmed",

                "address_street" => "required",
                "address_number" => "required",
                "address_city" => "required",
                "address_postcode" => "required",
                "address_country" => "required",

                "phonenumber" => "required",
                "birthdate" => "required|date_format:\"d-m-Y\""
            ];
            $this->validate($request, $checks);

            $user = new Learner();
            $user->name_first = $request->input('name_first');
            $user->name_last = $request->input('name_last');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->address_street = $request->input('address_street');
            $user->address_number = $request->input('address_number');
            $user->address_postcode = $request->input('address_postcode');
            $user->address_city = $request->input('address_city');
            $user->country = $request->input('address_country');
            $user->phonenumber = $request->input('phonenumber');
            $user->birthdate = strtotime($request->input('birthdate'));
            $user->save();

            $learner = Auth::guard('learner')->attempt([
                'email' => $request->input('email'),
                'password' => $request->input('password')
            ]);
            if ($learner) {
                \Session::set('user', Auth::guard('learner')->user());
                return redirect('signup/form');
            }
        }

        return redirect('signup/auth')->with('error', 'Er is iets mis gegaan, probeer het opnieuw');
    }

    public function getForm($month)
    {
        $this->middleware('auth:learner');
        $type_id = session('type');
        if (!$type_id) {
            return redirect('signup')->with('error', 'De sessie is verlopen, probeer het opnieuw.');
        }
        $type = CourseType::find($type_id);
        $courses = Course::where(function ($query) use ($type_id, $month) {
            $query->where('type_id', $type_id);
            $query->where('date_start', '>', Carbon::now());
            $query->where('date_start', '>=', Carbon::createFromDate(date('Y'), $month, 1));
            $query->where('date_start', '<', Carbon::createFromDate(date('Y'), $month + 1, 1));
        })->orderBy('date_start')->get();
        return view('signup.form', ['courses' => $courses, 'type' => $type]);
    }

    public function postForm(Request $request)
    {
        $this->middleware('auth:learner');
        $checks = [
            "course" => "required|exists:courses,id",
            "accept_av" => "accepted"
        ];
        $this->validate($request, $checks);

        $user = Auth::guard('learner')->user();
        $course = Course::find($request->input('course'));
        $job = new LearnerSignupJob($user, $course);
        $this->dispatch($job);

        return redirect('signup/complete')->with('completed', true);
    }

    public function getComplete()
    {
        if (!session('completed')) {
            return redirect('signup')->with('error', 'De sessie is verlopen, probeer het opnieuw.');
        }
        return view('signup.complete');
    }
}
