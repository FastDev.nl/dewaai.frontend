<?php

namespace App\Http\Controllers\Learner;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Learner;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function getIndex()
    {
        return redirect('learner/calendar');
    }

    public function getCalendar()
    {
        $user = Auth::guard('learner')->user();
        $courses = $user->courses()->where('date_start', '>', Carbon::now())->get();

        $events = [];
        foreach ($courses as $cou) {
            $events[] = [
                "title" => $cou->name,
                "start" => date('Y-m-d', strtotime($cou->date_start)) . " 13:00",
                "end" => $cou->date_end,
                "data" => [
                    "name" => $cou->name,
                    "type" => $cou->type->name,
                    "date_range" => $cou->date_range
                ]
            ];
        }

        return view('learner.calendar', ['courses' => $courses, 'events' => $events]);
    }

    public function getInvoices()
    {
        $user = Auth::guard('learner')->user();
        $invoices = $user->invoices()->orderBy('created_at', 'DESC')->get();
        return view('learner.invoices', ['invoices' => $invoices]);
    }

    public function getDetails()
    {
        $user = Auth::guard('learner')->user();
        return view('learner.details', ['user' => $user]);
    }

    public function postDetails(Request $request)
    {
        $checks = [
            "name_first" => "required",
            "name_last" => "required",
            "email" => "required|email",

            "address_street" => "required",
            "address_number" => "required",
            "address_city" => "required",
            "address_postcode" => "required",
            "country" => "required",

            "phonenumber" => "required",
            "birthdate" => "required|date_format:\"d-m-Y\""
        ];
        if ($request->input('password')) {
            $checks["password"] = "required|confirmed";
        }
        $this->validate($request, $checks);

        $user = Auth::guard('learner')->user();
        $user->name_first = $request->input('name_first');
        $user->name_last = $request->input('name_last');
        $user->email = $request->input('email');
        if ($request->input('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->address_street = $request->input('address_street');
        $user->address_number = $request->input('address_number');
        $user->address_postcode = $request->input('address_postcode');
        $user->address_city = $request->input('address_city');
        $user->country = $request->input('country');
        $user->phonenumber = $request->input('phonenumber');
        $user->birthdate = strtotime($request->input('birthdate'));
        $user->save();

        return redirect('learner/details')->with('message', 'De gegevens zijn aangepast.');
    }
}
