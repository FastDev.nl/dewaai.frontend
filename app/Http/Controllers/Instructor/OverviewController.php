<?php

namespace App\Http\Controllers\Instructor;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function getIndex()
    {
        return redirect('instructor/calendar');
    }

    public function getCalendar()
    {
        $user = Auth::guard('instructor')->user();
        $courses = $user->courses()->where('date_start', '>', Carbon::now())->get();

        $events = [];
        foreach ($courses as $cou) {
            $events[] = [
                "title" => $cou->name,
                "start" => date('Y-m-d', strtotime($cou->date_start)) . " 13:00",
                "end" => $cou->date_end,
                "data" => [
                    "name" => $cou->name,
                    "type" => $cou->type->name,
                    "date_range" => $cou->date_range,
                    "id" => $cou->id
                ]
            ];
        }

        return view('instructor.calendar', ['courses' => $courses, 'events' => $events]);
    }

    public function getDetails()
    {
        $user = Auth::guard('instructor')->user();
        return view('instructor.details', ['user' => $user]);
    }

    public function postDetails(Request $request)
    {
        $this->validate($request, ["password" => "required|confirmed"]);

        $user = Auth::guard('learner')->user();
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return redirect('learner/details')->with('message', 'De gegevens zijn aangepast.');
    }

    public function getCourse(Course $course) {
        return view('instructor.course_info', ['course' => $course]);
    }
}
