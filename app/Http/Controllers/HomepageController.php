<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMail;
use App\Models\Ship;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function getIndex()
    {
        $ships = Ship::all()->get();
        return view('infopage.home', [
            'ships' => $ships
        ]);
    }

    public function postContact(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        \Mail::to('info@dewaai.nl')->send(new ContactFormMail(
            $request->name,
            $request->email,
            $request->message
        ));

        return redirect('contact')->with('message', 'Het bericht is verzonden.');
    }
}
