<?php

namespace App\Jobs;

use Mail;
use App\Models\Course;
use App\Models\CourseLearnerAssignment;
use App\Models\Invoice;
use App\Models\InvoiceLine;
use App\Models\Learner;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\SignupCompletedNotification;

class LearnerSignupJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $learner;
    protected $course;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Learner $learner, Course $course)
    {
        $this->learner = $learner;
        $this->course = $course;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->setAssignment();
        $this->createInvoice();
        $this->sendEmail();
    }

    private function setAssignment()
    {
        $assignment = new CourseLearnerAssignment();
        $assignment->course_id = $this->course->id;
        $assignment->learner_id = $this->learner->id;
        $assignment->status = 0;
        $assignment->save();
    }

    private function createInvoice()
    {
        $invoice = new Invoice();
        $invoice->course_id = $this->course->id;
        $invoice->learner_id = $this->learner->id;
        $invoice->status = 0;
        $invoice->save();

        $invoice->invoice_code = md5($invoice->id);
        $invoice->invoice_hash = md5(rand(1000, 9999) . time());
        $invoice->save();

        $course_line = new InvoiceLine();
        $course_line->invoice_id = $invoice->id;
        $course_line->text = sprintf("[%s] Van %s", $this->course->name, $this->course->date_range);
        $course_line->amount = 1;
        $course_line->price = $this->course->price;
        $course_line->save();
    }

    private function sendEmail() {
        $this->learner->notify(new SignupCompletedNotification($this->course));
    }
}
