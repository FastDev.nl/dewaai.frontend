<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SignupCompletedNotification extends Notification
{
    use Queueable;

    private $course;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($course)
    {
        $this->course = $course;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function toMail($notifiable)
    {
        $url = url('learner/calendar');
        $intro = "U heeft zich ingeschreven voor de cursus ".$this->course->name." van ".$this->course->date_range.".";
        return (new MailMessage)
                    ->subject('Inschrijving ontvangen')
                    ->line($intro)
                    ->action('Inschrijving bekijken', $url)
                    ->line('Uw inschrijving zal zo snel mogelijk gecontroleerd en geaccepteerd of gewijzigd worden.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
