<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Wachtwoord resets')
            ->line('U ontvangt dit bericht omdat u een wachtwoord reset heeft aangevraagd.')
            ->line('Klik op de volgende link om uw wachtwoord te resetten:')
            ->action('Reset wachtwoord', url('password/reset', $this->token))
            ->line('Als u geen wachtwoord reset heeft aangevraagd, kunt u deze e-mail negeren.');
    }
}
