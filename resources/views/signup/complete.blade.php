@extends('layout')

@section('page-title', 'Inschrijven -')
@section('page-content')
    <div class="grid__item information">
        <h1 class="infomaintitle ">Inschrijving compleet</h1>
    </div>
    @if(session('error'))
        <div class="gird__row alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="container">
        <div class="alert alert-success">
            Je hebt je nu ingeschreven. De inschrijving kun je terug vinden in
            <a href="{{ url('learner') }}">Mijn Account</a>.
        </div>
    </div>
@endsection
