@extends('layout')

@section('page-title', 'Inschrijven -')
@section('page-content')
    <div class="grid__item information">
        <h1 class="infomaintitle ">Inschrijven</h1>
    </div>
    @if(session('error'))
        <div class="gird__row alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @foreach($tables as $table)
        <div class="grid__row tablemargin">
            <table class="table">
                <tr>
                    <th>Niveau</th>
                    @foreach($table['headers'] as $h)
                        <th>{{ $h }}</th>
                    @endforeach
                </tr>
                <tr>
                    <th>Leeftijd
                    </td>
                    @foreach($table['ages'] as $h)
                        <td>{{ $h }}</td>
                    @endforeach
                </tr>
                <tr>
                    <th>Overnachting en eten inbegrepen
                    </td>
                    @foreach($table['sleep_food'] as $h)
                        <td>{{ $h }}</td>
                    @endforeach
                </tr>
                <tr>
                    <th>Soort boot
                    </td>
                    @foreach($table['ships'] as $h)
                        <td>{{ $h }}</td>
                    @endforeach
                </tr>
                <tr>
                    <th>Prijs
                    </td>
                    @foreach($table['prices'] as $h)
                        <td>&euro;{{ number_format($h, 2, ',', '.') }}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    @foreach($table['ids'] as $h)
                        <td>
                            <form method="POST" action="{{ url('signup') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="course_id" value="{{$h}}">
                                <input type="submit" value="Inschrijven">
                            </form>
                        </td>
                    @endforeach
                </tr>
            </table>
            <br/>
        </div>
    @endforeach
@endsection
