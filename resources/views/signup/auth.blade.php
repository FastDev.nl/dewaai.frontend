@extends('layout')

@section('page-title', 'Inschrijven -')
@section('page-content')
    <div class="information">
        <div class="container">
            <h1>Inschrijven voor een {{ $type->name }} cursus</h1>
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>- {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-4">
                    <h3>Inloggen</h3>
                    <form method="POST" action="{{ url('signup/auth') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="action" value="login">
                        <label for="email">E-mail adres:</label>
                        <input type="text" id="email" name="email" class="form-control" placeholder="jan@jansen.nl">

                        <label for="password">Wachtwoord:</label>
                        <input type="password" id="password" name="password" class="form-control"
                               placeholder="Wachtwoord">
                        <br/>
                        <input type="submit" class="btn btn-primary pull-right" value="Inloggen">
                    </form>
                </div>
                <div class="col-md-6 col-md-offset-2">
                    <h3>Heeft u nog geen een account?</h3>
                    <form method="POST" action="{{ url('signup/auth') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="action" value="register">
                        <div class="row">
                            <div class="col-md-5">
                                <label for="name_first">Voornaam:</label>
                                <input type="text" name="name_first" id="name_first" class="form-control"
                                       placeholder="Jan" value="{{ old('name_first') }}">
                            </div>
                            <div class="col-md-7">
                                <label for="name_last">Achternaam:</label>
                                <input type="text" name="name_last" id="name_last" class="form-control"
                                       placeholder="Jansen" value="{{ old('name_last') }}">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="email">E-mail adres:</label>
                                <input type="text" name="email" id="email" class="form-control"
                                       placeholder="jan@jansen.nl" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="password">Wachtwoord:</label>
                                <input type="password" name="password" id="password" class="form-control"
                                       placeholder="Wachtwoord">
                            </div>
                            <div class="col-md-6">
                                <label for="password_confirmation">Wachtwoord confirmatie:</label>
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                       class="form-control"
                                       placeholder="Wachtwoord">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-8">
                                <label for="address_street">Straat:</label>
                                <input type="text" name="address_street" id="address_street" class="form-control"
                                       placeholder="Jansweg" value="{{ old('address_street') }}">
                            </div>
                            <div class="col-md-4">
                                <label for="address_number">Huisnummer:</label>
                                <input type="text" name="address_number" id="address_number" class="form-control"
                                       placeholder="42" value="{{ old('address_number') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <label for="address_city">Woonplaats:</label>
                                <input type="text" name="address_city" id="address_city" class="form-control"
                                       placeholder="Amsterdam" value="{{ old('address_city') }}">
                            </div>
                            <div class="col-md-4">
                                <label for="address_postcode">Postcode:</label>
                                <input type="text" name="address_postcode" id="address_postcode" class="form-control"
                                       placeholder="6811 AA" value="{{ old('address_postcode') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="address_country">Land:</label>
                                <input type="text" name="address_country" id="address_country" class="form-control"
                                       value="Nederland" value="{{ old('address_country') }}">
                            </div>
                            <div class="col-md-6">
                                <label for="phonenumber">Telefoonnummer:</label>
                                <input type="text" name="phonenumber" id="phonenumber" class="form-control"
                                       placeholder="06-32109876" value="{{ old('phonenumber') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="birthdate">Geboortedatum (dd-mm-yyyy):</label>
                                <input type="text" name="birthdate" id="birthdate" class="form-control"
                                       placeholder="28-01-2001" value="{{ old('birthdate') }}">
                            </div>
                        </div>
                        <br/>
                        <input type="submit" class="btn btn-primary pull-right" value="Registreren">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
