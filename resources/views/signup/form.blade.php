@extends('layout')

@section('page-title', 'Inschrijven -')
@section('page-content')
    <div class="information">
        <div class="container">
            <h1>Inschrijven voor een {{ $type->name }} cursus</h1>
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                            <li>- {{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class=col-md-3>
                <ul class="list-group">
                    <li class="list-group-item"><a href="{{ url('signup/form/1') }}">januari</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/2') }}">februari</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/3') }}">maart</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/4') }}">april</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/5') }}">mei</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/6') }}">juni</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/7') }}">juli</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/8') }}">augustus</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/9') }}">september</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/10') }}">oktober</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/11') }}">november</a></li>
                    <li class="list-group-item"><a href="{{ url('signup/form/12') }}">december</a></li>
                </ul>
            </div>
            <div class=col-md-9>
                <form method="POST" action="{{ url('signup/form') }}">
                    {!! csrf_field() !!}
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Datum cursus</th>
                            <th>Plekken vrij</th>
                            <th>Prijs</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($courses as $c)
                            <tr>
                                <td>
                                    <input
                                            type="radio"
                                            name="course"
                                            id="course-option-{{ $c->id }}"
                                            value="{{ $c->id }}"
                                            @if(!$c->places_left)
                                            disabled="disabled"
                                            @endif
                                            data-date="{{ $c->date_range }}"
                                            data-price="{{ number_format($c->custom_price, 2, ',', '.') }}"
                                    >
                                </td>
                                <td>{{ $c->date_range }}</td>
                                <td>({{ $c->places_left }} plekken vrij)</td>
                                <td>{{ number_format($c->price, 2, ',', '.') }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if(!count($courses))
                        <p>Sorry, er zijn geen cursussen voor de geselecteerde maand</p>
                    @else
                        <p>
                            U schrijft zich in voor een cursus van <span id="date-range"></span>.
                            Deze cursus kost &euro;<span id="price"></span>.
                        </p>


                        <p>
                            <input type="checkbox" name="accept_av" id="accept_av">
                            <label for="accept_av">Ik ga akkoord met de </label>
                            <a href="{{ url('/algemene_voorwaarden.pdf') }}" target="_blank">Algemene voorwaarden</a>
                        </p>

                        <input type="submit" class="btn btn-primary pull-right" value="Inschrijven">
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script type="text/javascript">
        var default_price = "{{ number_format($type->price, 2, ',', '.') }}";
        var updateStats = function () {
            console.log('waah');
            var price = default_price;
            var selected = $('[name=course]:checked');
            if (selected.attr('data-price') !== '0,00') {
                var price = selected.attr('data-price');
            }
            var date_range = selected.attr('data-date');
            $('#date-range').text(date_range);
            $('#price').text(price);
        };
        $(document).ready(function () {
            $('[name=course]:first').attr('checked', 'checked');
            $('[name=course]').change(updateStats);
            updateStats();
        });
    </script>
@endsection
