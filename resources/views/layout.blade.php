<!DOCTYPE html>
<html>
<head>
    <title>@yield('page-title') Zeilschool De Waai</title>
    @if(!isset($is_home))
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    @endif
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
    @yield('page-head')
</head>

<body>
<div class="grid">

    <div class="grid__item">
        @if(isset($is_home) && $is_home)
            <img src="{{ asset('img/mainimg_3.jpg') }}" class="header__img">
        @else
            <img src="{{ asset('img/mainimg_3.jpg') }}" class="croppedheader__img">
        @endif
        <ul class="menu navlist">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('signup') }}">Inschrijven</a></li>
            <li><a href="{{ url('information') }}">Informatie</a></li>
            <li><a href="{{ url('contact') }}">Contact</a></li>
            @if(Auth::guard('learner')->user())
                <li><a href="{{ url('learner') }}">Mijn account</a></li>
            @elseif(Auth::guard('instructor')->user())
                <li><a href="{{ url('instructor') }}">Mijn account</a></li>
            @else
                <li><a href="{{ url('auth/login') }}">Inloggen</a></li>
            @endif
        </ul>
        @if(isset($is_home) && $is_home)
            <img src="{{ asset('img/logo.png') }}" class="logo align__item">
            <div class="aanbieding"><a href="{{ url('signup') }}">Bekijk de prijzen!</a></div>
        @endif
    </div>

    @yield('page-content')

    <img src="{{ asset('img/mainimg_2.jpg') }}" class="follow__img">
    <div class="banner">
        <div class="grid__row">
            <div class="grid__item2">
                <ul class="footerlist">
                    <li class="footerupper"><a href="#">pagina's</a></li>
                    <li class="footerli"><a href="#" class="information">inschrijven</a></li>
                    <li class="footerli"><a href="#" class="information">informatie</a></li>
                    <li class="footerli"><a href="#" class="information">contact</a></li>
                    <li class="footerli"><a href="#" class="information">inloggen</a></li>
                </ul>
                <ul class="footerlist">
                    <li class="footerupper"><a href="#">social media</a></li>
                    <li class="footerli"><a href="#" class="information">facebook</a></li>
                    <li class="footerli"><a href="#" class="information">twitter</a></li>
                    <li class="footerli"><a href="#" class="information">instagram</a></li>
                </ul>
            </div>
            <div class="grid__item2">
                <ul class="footerlist">
                    <li class="footerupper"><a href="#">meer info</a></li>
                    <li class="footerli">
                        <p>
                            Zeilschool de Waai is een unieke school in  waar je op een leuke manier leert
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js" type="text/javascript"></script>
@yield('page-scripts')
</body>

</html>
