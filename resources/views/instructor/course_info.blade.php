@extends('instructor.layout')

@section('learner-content')
    <h2>{{ $course->name }}</h2>
    <p>
        Deze cursus is van {{ $course->date_range }}.
    </p>
    <h3>Cursisten</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Cursist</th>
            <th>Leeftijd</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($course->learners_assignments as $assignment)
            <tr>
                <td>{{ $assignment->learner->name_full }}</td>
                <td>{{ $assignment->learner->birthdate->age }}</td>
                <td>{{ $assignment->status_string }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h3>Boten</h3>
    <table class="table">
        <thead>
        <tr>
            <th>Type</th>
            <th>Naam</th>
            <th>Maximaal aantal personen</th>
        </tr>
        </thead>
        <tbody>
        @foreach($course->ships as $ship)
            <tr>
                <td>{{ $ship->type->name }}</td>
                <td>{{ $ship->name }}</td>
                <td>{{ $ship->max_person }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
