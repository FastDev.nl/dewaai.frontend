@extends('instructor.layout')

@section('learner-content')
    <h2>Mijn gegevens</h2>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>- {{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <form method="POST" action="{{ url('instructor/details') }}">
        {!! csrf_field() !!}
        <input type="hidden" name="action" value="register">
        <div class="row">
            <div class="col-md-5">
                <label for="name_first">Voornaam:</label>
                <input type="text" name="name_first" id="name_first" class="form-control"
                       placeholder="Jan" value="{{ old('name_first', $user->name_first) }}" disabled="disabled">
            </div>
            <div class="col-md-7">
                <label for="name_last">Achternaam:</label>
                <input type="text" name="name_last" id="name_last" class="form-control"
                       placeholder="Jansen" value="{{ old('name_last', $user->name_last) }}" disabled="disabled">
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <label for="email">E-mail adres:</label>
                <input type="text" name="email" id="email" class="form-control"
                       placeholder="jan@jansen.nl" value="{{ old('email', $user->email) }}" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="password">Wachtwoord:</label>
                <input type="password" name="password" id="password" class="form-control"
                       placeholder="Wachtwoord">
            </div>
            <div class="col-md-6">
                <label for="password_confirmation">Wachtwoord confirmatie:</label>
                <input type="password" name="password_confirmation" id="password_confirmation"
                       class="form-control"
                       placeholder="Wachtwoord">
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-8">
                <label for="address_street">Straat:</label>
                <input type="text" name="address_street" id="address_street" class="form-control"
                       placeholder="Jansweg" value="{{ old('address_street', $user->address_street) }}" disabled="disabled">
            </div>
            <div class="col-md-4">
                <label for="address_number">Huisnummer:</label>
                <input type="text" name="address_number" id="address_number" class="form-control"
                       placeholder="42" value="{{ old('address_number', $user->address_number) }}" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <label for="address_city">Woonplaats:</label>
                <input type="text" name="address_city" id="address_city" class="form-control"
                       placeholder="Amsterdam" value="{{ old('address_city', $user->address_city) }}" disabled="disabled">
            </div>
            <div class="col-md-4">
                <label for="address_postcode">Postcode:</label>
                <input type="text" name="address_postcode" id="address_postcode" class="form-control"
                       placeholder="6811 AA" value="{{ old('address_postcode', $user->address_postcode) }}" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="country">Land:</label>
                <input type="text" name="country" id="country" class="form-control"
                       value="{{ old('country', $user->country) }}" disabled="disabled">
            </div>
            <div class="col-md-6">
                <label for="phonenumber">Telefoonnummer:</label>
                <input type="text" name="phonenumber" id="phonenumber" class="form-control"
                       placeholder="06-32109876" value="{{ old('phonenumber', $user->phonenumber) }}" disabled="disabled">
            </div>
        </div>
        <br/>
        <input type="submit" class="btn btn-primary pull-right" value="Aanpassen">
    </form>
@endsection
