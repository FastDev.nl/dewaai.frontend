@extends('instructor.layout')

@section('learner-content')
    <h2>Agenda</h2>
    <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Agenda</a></li>
    <li><a data-toggle="tab" href="#menu1">Tabel</a></li>
  </ul>
<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div id='calendar'></div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <table class="table">
        <thead>
        <tr>
            <th>Soort Cursus</th>
            <th>Naam</th>
            <th>Datum</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
      @foreach($courses as $cou)
        <tr>
          <td>{{ $cou->type->name }}</td>
          <td>{{ $cou->name }}</td>
          <td>{{ $cou->date_range }}</td>         
          <td><a href="{{ url('instructor/course/'.$cou->id) }}">Informatie</a></td>
      </tr>
      @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection

@section('page-head')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css">
@endsection
@section('page-scripts')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                events: {!! json_encode($events) !!},
                eventClick: function (event) {
                    window.location.href = "{{ url('instructor/course') }}/" + event.data.id
                }
            });
        });
    </script>
@endsection
