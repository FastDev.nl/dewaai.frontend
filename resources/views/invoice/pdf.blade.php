<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Factuur #{{ $invoice->id }}</title>

    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2) {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
    </style>
</head>

<body>
<div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title">
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAogAAABpCAYAAAC5ztK0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAABn4SURBVHja7J3NchpJtsdPIYEE+rCQpufajv5kthOzoR8BPwJ+BHp5lzjuYhYTdyE2d28eQTyC9QgmYhazNRE35kZb7VYLybKEQIi8C1WiQ7mAyqrMyiz4/yIq3G0XlR+VdfKfJ09mekIIAgAAAAAAQJJDFQAAAAAAAAhEAAAAAAAAgQgAAAAAACAQAQAAAAAABCIAAAAAAIBABAAAAAAAWtnMYqabXSIiCtufx3Mhf60qGhYAAAAA1lQgfvr0yVK2/4w3BwAAAADgokDs9/sQiAAAAAAAEIhP5HIIYQQAAAAAgEB0G2fjEgEAAAAAIBDdFI0QiwAAAAAAEIgQiwAAAAAAEIgQiwAAAAAAscEqkyexKFANAAAAAAAQiGFCEQAAAAAAAhEAAAAAAADJ5pqXH7GHAAAAAAAQiAuFoYCQBAAAAAAE4nqLwqgxhwJCEQAAAADrwrrEIHohgi/OghQsYgEAAADAyrPqHkTPgMATBE8iAAAAAFaYVfIg3jPh5gVEnO59DuFJBAAAAAAEYgYYM3FoShhCJAIAAAAAAjHDZYGAAwAAAACIQaIYxI2NDZfKMrEgDBGPCAAAAAAIRM7Ozo6dXPdD/9aWNxQiEQAAAAArBY7aAwAAAAAAKysQJxbTdi7e0fO8Y8/zRIrXied55PjF6+R9hPvr7P5GBspn4uJ1UF3TOliFd7cK7becMXuT5Gqwsn4w3Daairbz2EB+Phgsq7NXjL78PQQi0EEZVbCQvuH7Achye3e1DLIcvRV/X21W1op/6aTD6rAW4f46ewctzXlpsvK18am68x1DIEZ0yIVcc72Io9Eo1qXVpSnEL0IIb9k1p2xRr79kzGBXIBATDTQgktGxgPTohAg0nZz6f1aXOBQaAQGnuy3VWR/SwmvX2m8lYhN1HyoGVe6TwnA6xf3rr7/GSvjHH3/MWl2d+Ialn5GR32t0sBCIEIgoT4YEYsP/75oB8SSfX/ZFWnuBQJR1rjsPdV+gEsF7GIoQ4rWttCEQlwtDseT+r343mUzWob7esg/7Da3mlA+m6QHaL7DFqW9XK8zWpv38WkDA6RbmTf9PeA8dZJUEYpzpck9BEM6776tnHB0drXq7abBRZXuFR368g+3RetMneBDRfkHatIno2H+XDQO2Vj6/TkS/zLH18vvXLeCqBO8hBGLKTCKIxSTCcKlQfPbs2Sq3mYpvUIiIunOMCgAAmGRdBO8p+++aASF1ygYTtUB6FXqKD4T3cA1ZxaP2ckuEYdhZzTq2qVmXo/143OHrNSnzOnvPME2J9gvs0fUvKRBNPj+4EMa095CLTwCBaK0s84ShblZdJK5D3CEIB+8aADt02GCtYfD5QQGahvcwKwscIRAzzniBODQtDNeBdYk7DAPeF4D2a58yPXm012nAwre7qRl8foU9X+5NmIb3EPbVURLFIOZyOXp4eIi8I3jKBDOVljBcxbOZ04g7PGZCNGxaUxqpNyl3SHE72DIzsvU5nXabGcmkHV6TnuKIFq14bLH0VQx/P2L61QWdmOwMuoFOTzeyA4pS962UOigbeUrSftMYcFaWfPNSvPQCbXfV7U6QHj3GBtYMCcSe/03Kb/eUtdFTMrfvYRLx2WQDhkVe1VO/bLqFru300xFRQsTXTaPRiL58+UL39/epZvp//u8/wgTfgIhKcwRbqrSq2RGIEcX9e9949InoZ42jdyk8VTeB7VC8Ke5jepraOIxg+OT9Xb/cUTvltzHK9DqGaKr6+Uuyie6yepB10KGvY04r/r/FmfaSBlO30ebCIyqnfvlMeMVt5ilO+zUtCuuaRM7P9BQ757rd0dWOpJj9xUBblc/v+WU80VDP8+zjB//PloLwlgPuasL20/Hrrx8j39rTj6K/PM+b6beEEKkN+BJ5EIUQVCwWqVQq0eXlpW1PYtG2MJT88ccfsX7n6PY4puIOucHjHpR5I61j5n2RlwlDOW+EHbXjecfEgBw9vlkyCl12ksEyscu9HYtGqnxqp8yES3dJmaK8P1nWed5BmZY0sGX/9w1feCbphBr0tBVIlLqQm/NWmVem5v/9a00eE5fyZHs6VoanVEPaq+w0uwt+WyF98XdZsjtBYSHzbWI1c4eV9TjCe0lS/2VS96h9oFmPeDsw0FyWHt8QvEZErxTLZjt9Ow6kJB7E4XBIDw8PVCqVaDAY0NXVVSoicY4H0XNBHBIR/eeLX2NVwsuXL9NvAIvfV8MXiFIAvNKU7Ds2CuuSWkxjxc9TjYnWqIZGiqq+7zlbxon/QYd5z8KQntaeBtETtf5MeOLC6qBNT6EFJzQbQ/RGUcAEPTj9BAbzLRMOPT8/KnUR9PjqeHeu5Em1/Zpuq8S+9Tj1W/W/MaJoMwCu2B3ddRnVfiV9V654D2XddynZVD8fHKjMhhlLXwixNH3mQewLIQ7TbHDaFqkUi0V69uwZCSFoMpmQEMLYtQCdi1CWnb08PxP6y2UDPpLsaexguBFq+x+pymi454uJFhN9NcN1EaUjOqanaXiTo0Nef6f0eB52Gh1Vn4mfuv8efqZ40zWyPb1mHcdJTPHaYN6OOHXRZ3mRp0qckPqUsMt56luyH+8DbfXQby82PCdZtDtBTK9m5nsgdg28J+lFU/UeyrpPGgfaCtic44ykbw2tq5iLxSIdHBxMp51NXQvEoUlBGFksPjw8xLocQ+53SDFFwDzPCh+BJ1ns8oYZtLcxBIruDpZ71ExN6fH66/hGK83OX04V9jR5FzrM6Mp4RpXReJ21hdca8vIqIMjieAhcy5MNgShDLarMfuhoq5WM2x0dArEfsDc66YaIUd32QwpRWwunOkyc1jKWfuqhItq3udne3qaDgwPa3983ds0Rb0mFobbf5PP5WJdDBOMOTzU8s84MhK4pURmbFVVcqHYwUeMCK+zZprwjNVZ/XUp/yrBPT9PzOoVpixm+qJ1elY2+26TPg9pj4qGqKFhdzJONTc6lN7jCvlGbW2K5YHd0foNd1hZ0v99GwN7opMnahO3VvMHTY6yl73lelPQrtipqVfZBjOM9jCMMIwnTjE8xNwIjPV0fM98mR9eWETxYWGVEHXUkFnWbkDQ6Yh67kqY4LLP6rZAZD2lbsdN7GyKedBrvTkiHmcU82djmhg8ufyEzniiVk2Fcsjs64NPMOtOuBp5XNSQ+TSx8UaVrWXzFTT/7HsRljMdjmkwmOh+p1funI/2jo6NYlwOYijtsGBw9njKDZmuk1TNscPjehu2UDUWZ1a+pvbxUDCavC1N707VYXhqK78eVPNmgTrOhFrY3019Fu2Nqmvk4YMt0xjm65D20Mbh3OX23BOL9/T2dn58nFYlizn+nKQznisStra1YlwOYiDvkhqxnwKPA41lqET/GvuaPt886DBMeBR0by+rA1KkHfQWB2DDYlrhg7Sq8TxfzZKPz4SLDxGb65ZjfjW27o/tb4eK0rMm+8BjNU83lcsl7SDTrHe1nIH1rG94n2gfx7/+KI2qKj9e/tQkzEeEeHWJ01U5HCWIi7lB2+Hz1oAl6EY2lKYEoyyY9SQ3NZa2HeA9scGrw/UXFdFsiVtdVijbV5mKe0haIdSbuTdWDSnlcsjsmvsM6PU0zJy1fkw1COqzu5H6lfU3twhXvYdOyQFRN35pAzGoMomdYHAoK3zJn3t/zPGVRSJqKOwyOQk2NHvshIzMTz1/WefPVjTWN9Vdmadiib1AgxqkL056ILjPOtYzlKU771WFDpGhqpdAW18HuLBqM6ppmrrP8twLP1xHn2GS2ywXvYZPsLExxJf21EIhRRWRcYRj13lXAVNwhBYynSYHRd+R+uW8d0eN0fTUj9besfZgWP3xauedIXUSNK3UxT2kLRO6tS2MQ03fku3HBm5/Ugxn0HspyqYYzLBOfNge3xGzycYbThwdRUczNE4ZejGeJCPcY2azw4eFB98Id1UZrIu4waKjLNOuB1XlFNWIqnWo5huGVK4zl6Pu9hhF4NYJwSoOeA998LQWxysvbj9BuXMxT3Pab9L0Q2fcyu2h3TArEJItJwryHFBB0SQRomPi05QDh76uVol3TkT62udGECWG4SJSG/nY8Hke65NY2d3d3dH9/n35leZ6puEMnGrelkViXZvcJPKFke6VVUxQgLgjEfoS2lHZeyhnLky0xZtqLqhqDuMrwaea4U5aLBFyHDXTjCNDaAvFpmrovyj74V5OJ6r+kMIgxlX62FqlYRGgSh0bq6/z8fHHCQlA+n6fDw0PyPC+V86tDxKHJuMMwL4bK2ZsuCMRyAnHUpceTRt6xEWSF4q3sLKcsQOa9P9PpRHk3addFFDHmYp6Stl/XBzF9hXbrkt3RTce34TXfvqi85/oSASenmeViFdX+obFAfJpof7I8YWK5RbMxkFXN30Za6UMgxtE6mgSmNp4/f+52hXme6bhD1Y7fRbGTVBj1mEiUK5sr9OhddE2gRS2PaYG4aANkl+rC5Tzpar8qpBGnGmwnWbE7JjllQqyuKOKiTP92mEBUEaB8021T3sMGs6thA/RTMrswJpX0Pc+zamOyLhCDK5mXiUVBgMh83KGNTkqnB0JnOlIkSkP7jp7iFF2nvGLpxM1XD3mKJJL7jrSTMq0HfBpYxcu3zHsoafuOhLKiADUVeyg3iW+G2Fl5gEA74nOWDUhTSV8IoZI+FqkoCMMo29xAHH49Ikkj7tD10Xtf4SPWUY5XNHtA+zvKRoxUWp4YeOH056mSge/QVNlX2WsYJhKlXYlqU1QEnOqm2dx72Nb43k9oNp6v69vUQ/96Q2b337SZPgRiDFS2o4E4pK/jDoUQrVVr0IY6ex3leENPsVDVDInENN+la+3FZW9dL6P1moTKmpU3CnyAH2VVdVTvYZhAjFL/fD9MHYKpQUQXrGyn9Dgr87NvT5O8614G0rc6SM2t+McDYfgkDtOKOwwaaldWXZYN3x+FFqv3ii8SyxHrz+UOWde76TvUlqIEkruYJ3Lke7P5Dbtkd0zTYW0hipdPdfpXZVNuvrG2DnHYpMdDB2R+X9PjbExai6Fspw+BaFAYWhOHZ2dndHZ2Rh8/fqQvX764UiczcYeKsQ9J6IV0braxOXLr+KPPvi/ATiJ6Q2x7G/sOvbc06oLHKZ1mLE9pdixpiTF+Yk1Poa24ZHdMEdXLp+o95DYrikBs+u9Ix2k6NebQkF47XfGMUQakttOP229BIM7BI0emk+/u7uju7o5GoxHl83n7FROIOxRCpLmZbdcRQx23AzfhxevS03RzbYnhtVl/acUgViKmkWZdRN26xcU8mW6/8wYP1RTKnjW7kwadgAhcJOBk3aiIHX5qS2WBrZDTyzq8h8csr68Mt1sX00/ab621QBQR/84K3333HX377bf08uVL2ty0u1DcQtzhPEOten6sbUx7YNrMSDcj1F9lBetA1WDyujBdH/VA55ilPKX97k5TaKOqmzVn1e7Erf/eEoHIvYeq+7F2aPk0s27voclNtiuOp+8MWV7F7Cy3t7c0mUxoY2NjelqKJXFoI+5wXudBjhhq23F9QZG4bGTeDRmhrxrlGG3J5DFnDZanTgbzlDZcJJv4xlVW6Lpqd9Kys/NsSZO1nTgxdJ0lQl1n7CE/mcdkW+85mn5ov5ViWJhVgRg8zzLu750Vqg8PD1ZORwnBVtxh8CPohHRyaaPawaSRz1NaPj3HvQNpd3RRNrBOUyD2WEdoUiw3WKfYy2CeiOx4EE2J5AZrh1mzO2mxaJq5Qck9YtxLXA0RnxV62g9Qp0C0YW9sp5+k38q8QFwkGB9CBOQ4oaBMnYODA8rn87a9hzbjDoO02YfRpGzg0hYnbWb4VzmmqqdQFxVDbSlOZ+pintJsv1yM1TWLsbp/dWOUJYt2J4lInzfNnNR7KH877+xnHnuoQ1TZ3nDdlQ3frZPLQH42CKiKQ9txh2HGi8fb1fCWpoIiyoq2FjP+b1e0HlQ6qtOA50JnPo5jdKYu5iltWqzzO9H0zDIrexwbtm52R5aVTzPz9tjS9PxGQHxK76GufobvDGDCg7bM5tpOHwIRGBOHLsQdhvEL+/BOKH1PmOqoLQ3Xfp0ZimUe3hYz/u8cFG5pt6U+EyMVTe1DhmT0SD2Q37U8pf3uujR7WpAOj52sRyn0Khm0O2kPxIO2paFxcMGnmWshz+8bKIeJsI2y4+kn6bcyLxC9FfkYXS2HC3GHYfRZB1f2RU6aiy5cOxWDT0dGidtpUzaP64vyXlRH1FwsScGcpOOvEtF7/88+xTsr28U8pQ0/uvM4gUiU9qGmYZBr2+6kLRD5NLNO72FQBAafr/OYuS4Ts7o9v+UIost2+s70W/Agxse5eEjP86qsU2pbjjucZ8BeM0/LWw0dqamRWMXghykFRJlm90SM0gFzkfjBr8Oy4Tpz8RzmDs2eSvOe4k2/v/V/K6fJkpyU4FKeTLbfRbwKiETVgUzDL7sUh6+YvShn0O6kDZ9m1hF7GFaXUiA2DDxfwgdE70hfDGk5I+kntY9aSLpJ37ofZRdW/qln8b/+uRGxfkqPQ7xqYq8kN8QNPxbRNF163GVexYD1fCNd9TuC9/5z5Ah40Wj0OCC0qvR4WLqND63h54fnt7VAUDVp9jD7bgzvyBu/jo7paeubhl933QXGmgfqyyOxDiPWmYsCUbaln1lbknUhg+XD3kXFv6dCs8H88l0kFVQu5iltXtGTB1EOZGT7bIeUp86+i0qIqNMlnGzYHRsCsRn4tnTGoJ/S00Ikk9suycGBHEgf+99IZ8m7qjDhWmXtMayPdDn9lRCIIFw0eqiGSKKyyTpG7v1U8bqcWvzQuvT1CsmoI802qce58d+2mdGSmwGrxH65stiBG8y+prbUUHgX0ti3DLdv23lKmzd+Z3rM2maU9tll7ZsSdKyu2B0btrXH6ku3d6/NBqdE6qeyxHlfb+lpL8ym4rt6k6AfsJ2+dYHoJdmKpdldew9iKNITqFo/GjyI6g3Ajf0aVTpQvpWCK6s6jwPekMoSL8YbzelX2ai+sUB0dAICZBXhnqh5e/K1WFvqrWmebHwj89rnMu837A7gA/FFNrbLxLvOb8lI+ja3woNAhEAEAAAAAFAm6SIVCJqv68OLWT+oSwAAAAC4IWiSeBCHwyENBgO6vb1dOFUp08jl1PXoZDJ5yqyfhsrz5O/D8qdrelUIQcVikQ4ODujz5890cXFBpVKJvvnmG7q/v6fz8/O5aQkhqFQqUbFYpK2tLbRIAAAAAFjH6CKVyWRCuVyOSqUSTSYTury8VH7G4eEhbW5u0s3NDY3HY8rlcrS9vU2e59Hl5eVSkXd0dERCCLq5uSEhBHmeR5PJhCaTCY3HYy0xAEIIGo1GdHV1Rbe3t0REdHt7S5ubm7S7u0ue503TBgAAAABYS4EoxdDOzg7t7u7S5uYmXV1d0XA4VBJJQoipyCqVSnRzc0NbW1tUKBTo+vqaRqPR0ucVCgXa2tqinZ0dury8pJubm6lXcTQazXgokzAajUgIMePVHI/H5HkeHRwc0GAwoOFwCKEIAAAAAOfROsUsBVKxWKSdnR3a3HzUn83H9V6C1LeAmd7PF3Cw50UuZ6tKdHNzQ7/99hu97VdUf5+ojv/7b/fkeR6Nx2O6vb2lu7u7melyTDEDAAAAwCW0eRCl0Nzf36dCoUBERPf39/6/5qUYU3WdTe+/uLig7e1tX3QWVMWduLy89EajkRStaa6+Fnd3d54UhLJuhsMhWh8AAAAAVlcgyqngzc1Nuru7o8FgEBCOf6JcglnViSD69OkT7e3tUT6fJ6I/Kz/j999/p0KhQNvb26lXMvcYEj0ujtnc3KTxeIwWCAAAAIDVFYi7u7u0s7MT+u/e/9LDRNBGkjR++uknyufzj0Lr3+q//+GHH+jm5oaurq5Sr+Td3d2v68TzptPzAAAAAAArJxCXCsjHdJJM63qB+DxP8XleoVCg4XAoF6Xw308MFj1HRN7GxtfaOJfLTWM0AQAAAABcQssilS9fvlC5XJ7rQSQiurq6orOzM6UVvJPJhF6+fEn7+/uzglMIur6+prOzs6XP+P7776fTysPhkM7Ozmg8HlOhUKDBYBBpmtfzvOkVWRQLQUdHR3RwcBC6lY70IF5fX9POzg4WqQAAAADAGZx0YQkhSAhB+Xzejzn8WlwVi0V68eLF0mfx329tbdGLFy+m29s8e/YsUn7G4zFdXFzQ/f29slAEAAAAAIBA1CQM9/f3aW9vb65XbZ54XEahUJh6D7e2tiKf7lIqlej6+pqurq4gFAEAAAAAgZgGk8mECoUC7e3t0d7eHm1sbEwFowkhJoSYnvQSJW+5XI4ODg5ob2+Prq+v6fPnzzQajWIdHwgAAAAAAIEYgaOjo6kwlEfh/f1fWyb2K/QXqJQUfpIjogL9469DT56MIoXixcUFWhEAAAAAIBB1UywWSQgxc1bz48KOFyaSiy06Ly4upt5MOcVcLBZn9n0EAAAAAIBATIDnebS9vU2FQkHbmchpIae/t7a2piuSk6wIBwAAAABYe4E4mUxoZ2eHnj9/Tg8PD+E3/WpGl1JML2K5XA79+42NDfr48SPd3NwgJhEAAAAAEIhaFNucRSj/+OvQy+fzJkSXNxgMqFAoUNgm1mFi9vFcaaxaBgAAAMDqA3cXAAAAAACYYWXPehsMBjNT10IIur+/p8PDw+n/AwAAAACANRKI5+fnNBgMptPXQggqlUpTgWho6hoAAAAAAALRVZZtrh3nFBYAAAAAAAjEDNLsEhGRIPr263+8JqLfHvVjq4qXDwAAAAAQxirOsQpN9wAAAAAAQCACAAAAAAAAgQgAAAAAAGbwsN0LAAAAAADgwIMIAAAAAAAgEAEAAAAAAAQiAAAAAACAQAQAAAAAAHH4/wEAoi0zZCMpjmAAAAAASUVORK5CYII=
" style="width:100%; max-width:300px;">
                        </td>

                        <td>
                            Factuurnummer: #{{ $invoice->id }}<br>
                            Factuurdatum: {{ date('d-m-y', strtotime($invoice->created_at)) }}<br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            7001 DV<br>
                            Wilheminastraat, 50<br>
                            Doetinchem<br>
                            info@dewaai.nl
                        </td>

                        <td>
                            {{ $invoice->learner->name_full }}<br>
                            {{ $invoice->learner->address_postcode }} {{ $invoice->learner->address_city }}<br>
                            {{ $invoice->learner->phonenumber }}<br>
                            {{ $invoice->learner->email }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr class="heading">
            <td>
                Cursus
            </td>

            <td>
                Prijs
            </td>
        </tr>
        @foreach($invoice->lines as $item)
            <tr class="item">
                <td>
                    {{ $item->text }}
                </td>

                <td>
                    &euro;{{ number_format($item->price, 2, '.', ',') }}
                </td>
            </tr>
        @endforeach

        <tr class="total">
            <td></td>

            <td>
                Totaal: &euro;{{ number_format($invoice->total, 2, '.', ',') }}
            </td>
        </tr>
    </table>

    <p>Deze factuur moet voldaan worden 5 dagen voordat de cursus start.</p>

    <p>
        IBAN Nummer: NL 81 KNAB 982 901 827 3<br/>
        Op naam van: Zeilschool de Waai
    </p>
</div>
</body>
</html>
