@extends('layout')

@section('page-title', 'Inloggen -')
@section('page-content')
    <div class="row information">
        <div class="col-md-4 col-md-offset-4">
            <h3>Inloggen</h3>
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if(session('message'))
                <div class="alert alert-warning">
                    {{ session('message') }}
                </div>
            @endif
            <form method="POST">
                {!! csrf_field() !!}
                <label for="email">E-mail adres:</label>
                <input type="text" id="email" name="email" class="form-control">

                <label for="password">Wachtwoord:</label>
                <input type="password" id="password" name="password" class="form-control">
                <br/>
                <input type="submit" class="btn btn-primary btn-block" value="Inloggen">
            </form>

            <p style="text-align: center;"><a href="{{ url('password/reset') }}">Wachtwoord vergeten</a></p>
        </div>
    </div>
@endsection
