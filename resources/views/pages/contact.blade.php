@extends('layout')

@section('page-title', 'Contact -')
@section('page-content')
    <div class="container information">
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-6"><h2>Heeft u nog vragen?</h2>
                <p>
                    Als U nog een vraag aan ons heeft over iets wat niet op de website staat dan kunt U ons bereiken via
                    info@dewaai.nl. U kunt natuurlijk ook ons handige Contact form gebruiken die hiernaast staat. Wij
                    proberen dan zo snel mogelijk U te beantwoorden.
                </p>
            </div>
            <div class="col-md-6"><h2>Contact opnemen</h2>
                <form method="POST">
                    {!! csrf_field() !!}
                    <label for="name">Naam:</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Jan Jansen">
                    <label for="email">E-mail adres:</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="jan@jansen.nl">
                    <label for="message">Bericht:</label>
                    <textarea name="message" id="message" class="form-control"
                              placeholder="Ik vind jullie lief"></textarea>
                    <input type="submit" class="btn btn-primary pull-right" value="Versturen">
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"><h2>Connecties</h2>
                <p>
                    Bij bij de waai vinden het belangrijk dat we een goede connectie hebben met onze klanten. Daarom
                    kunt U op onze contact pagina alle manieren vinden om ons te bereiken. Via telefoon, email, social
                    media of als u dat wilt kunt u ook een brief schrijven.
                </p>
            </div>
            <div class="col-md-6"><h2>Contactgegevens</h2>
                <p>
                    1000AA Doetinchem<br/>
                    straatnaam 10<br/>
                    Zeilschool de waai<br/>
                    06-2345674<br/>
                    info@dewaai.nl
                </p>
            </div>
        </div>
    </div>
    <div class="grid__item mapmargin">
        <div class="map">
            <iframe width="100%" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2458.246265894253!2d6.295813951778316!3d51.965934979613486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c784c5ed2dc5b9%3A0xd68f886fe53531c5!2sDoetinchem%2C+Graafschap+College!5e0!3m2!1snl!2snl!4v1467912146598"></iframe>
            <br>
            <small>
                <a href="https://www.google.nl/maps/place/Doetinchem,+Graafschap+College/@51.965935,6.295814,17z/data=!3m1!4b1!4m5!3m4!1s0x47c784c5ed2dc5b9:0xd68f886fe53531c5!8m2!3d51.965935!4d6.298008"
                   style="color:#777777;text-align:left;font-size:13px;font-family: 'Source Sans Pro', sans-serif; padding:-20px;"></a>
            </small>
        </div>
    </div>
@endsection
