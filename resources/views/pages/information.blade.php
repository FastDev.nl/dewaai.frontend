@extends('layout')

@section('page-title', 'Informatie -')
@section('page-content')
    <div class="grid__row  information">
        <div class="grid__item2">
            <h1 class="infomaintitle ">Algemene informatie</h1>
            <p class="infomaintitle">Zodra je je hebt aangemeld bij een cursus staat je bij ons in het systeem en
                verwachten
                we jou op de zondag waar we wat gaan vertellen over de week. Je overnacht in een barrack met met de
                andere
                cursisten.<br/>
            </p>
            <p class="infomaintitle">
                <br/>Belangrijk is dat je de volgende voorwerpen meeneemt,<br/>
                -Kussensloop.<br/>
                -Toiletspullen.<br/>
                -Schone kleding.<br/>
                -Zaklamp.<br/>
                -Handoek.<br/>
                -Een goed humeur.<br/>
            </p>
            </p>
        </div>
        <div class="grid__item2">
            <h1 class="infomaintitle ">FAQ</h1>
            <h4 class="infomaintitle ">Moet je je eigen eten regelen?</h4>
            <p class="infomaintitle ">Nee hoor, dat word allemaal verzorgt.</p>
            <h4 class="infomaintitle ">Kan ik mijn inschrijving annuleren</h4>
            <p class="infomaintitle ">Als u uw inschrijving wilt verplaatsen of annuleren dan kan dat, bel dan tijdens
                werktijden naar 06-123456 of stuur een mail naar info@dewaai.nl</p>
            <h4 class="infomaintitle ">Vanaf welke leeftijd kunnen kinderen les krijgen?</h4>
            <p class="infomaintitle ">Kinderen vanaf 8 jaar kunnen meedoen aan de beginnerscurus!</p>
            <h4 class="infomaintitle ">Is er wifi op de boot?</h4>
            <p class="infomaintitle ">Nee.</p>
        </div>

    </div>


    <div class="grid__item">
        <h1 class="infomaintitle ">Informatie over boten</h1>
    </div>
    <div class="grid__row">

        <div class="grid__item2"><h2>16-kwadraats</h2>
            <p>De zestienkwadraat is ontworpen door Hendrik Bulthuis, kapper uit Bergum, die eerder de BM ontworpen had.
                Hij
                ontwierp de boot voor een prijsvraag in 1931, waarbij de eisen waren dat de boot eenvoudig gebouwd kon
                worden, en door iedereen kon worden gezeild. Bulthuis gebruikte voor zijn ontwerp dezelfde bouwmethode
                als
                voor de BM, waarbij de huid van de boot opgebouwd wordt uit vele smalle houten latten. Die bouwmethode
                maakt
                het mogelijk op een eenvoudige manier toch tamelijk complexe ronde vormen te maken. Door de overeenkomst
                in
                bouwmethode met het eerste ontwerp van Bulthuis wordt ook de zestienkwadraat wel "BM" genoemd, of ook
                "Vergrote BM". Het schip had oorspronkelijk 15,9 vierkante meter zeiloppervlak, wat in die dagen juist
                onder
                de grens was om vrij te blijven van de personeele belasting voor pleziervaartuigen
            </p>
        </div>
        <div class="grid__item2">
            <img src="{{ asset('img/16kwa.jpg') }}" class="infoimg">
        </div>
    </div>
    <div class="grid__row">
        <div class="grid__item2">

            <img src="{{ asset('img/draak.jpg') }}" class="infoimg">
        </div>
        <div class="grid__item2">
            <h2>Draak</h2>
            <p>In 1936 werd de eerste draak in Nederland geleverd. De klasse werd zeer populair in Nederland, evenals in
                de
                rest van Europa. In 1948 verwierf de klasse de Olympische status. Dit duurde tot en met de olympische
                spelen
                van 1972 in Duitsland. Desondanks steeg dit type wereldwijd snel in populariteit, vooral door de mooie
                elegante rompvorm, die niet is gewijzigd sinds 1929. Alleen de kajuit is meer gestroomlijnd, omdat
                moderne
                draken geen kooien meer hebben en uitsluitend voor races worden gebouwd. De tuigage en het zeilplan zijn
                door de jaren heen langzaam geëvolueerd.
            </p>
        </div>
    </div>
    <div class="grid__row">
        <div class="grid__item2"><h2>schouw</h2>
            <p>De Friese schouw varieert in lengte van 4,5 tot 6 meter, is 1,4 tot 1,8 meter breed en heeft een sterk
                hellend voorbord. Deze schouwen werden in Friesland op veel plaatsen gebouwd, niet alleen op de werven,
                maar
                ook vaak door particulieren en andere handwerklieden die ze voor eigen gebruik nodig hadden. Per plaats
                kenden de schepen een eigen uitvoering. Zo wordt wel gesproken van Terhornster, Eernewoudster en
                Grouwster
                schouwen. Ook de afmetingen en het tuig konden variëren. De Friese schouw werd veelal gebruikt voor het
                dagelijkse werk van kruidenier, boer en meelkoopman. Ook als plezierbootje was de schouw in trek. De
                schepen
                werden vaak fraai geschilderd. Soms werden ze versierd met verguld houtsnijwerk. Voor het wedstrijdvaren
                werden de schepen uitgerust met erg veel tuig. Ballast was dan nodig om ze overeind te houden. De
                Lemster
                schouw of spekbak heeft als voorbeeld de Friese schouw. Door economische redenen moesten de vissers op
                zoek
                naar de goedkoopste schepen. De schouw was de oplossing. De kistachtige, weinig elegante romp, met voor
                en
                achter een platte spiegel, wordt van brede planken gemaakt, die slechts in een richting gebogen hoeven
                te
                worden. De planken zijn van goedkoop grenenhout. Om toch de nodige zekerheid te krijgen werd de schouw
                al
                bij de bouw overijzerd.
            </p>
        </div>
        <div class="grid__item2">
            <img src="{{ asset('img/schouw.jpg') }}" class="infoimg">
        </div>
    </div>
@endsection
