@extends('layout', ['is_home'=>1])

@section('page-title', 'Home -')
@section('page-content')
    <div class="grid__row">
        <div class="grid__item2"><h1>De Waai</h1>
            <p>Zeilen leren bij Zeilschool De Waai is een fantastische ervaring. Een zeilvakantie bij ons is de garantie
                voor een fijne en veilige vakantie. Zeilen vinden wij de mooiste sport die er is. Zeilen is niet alleen
                omgaan met boot, water en wind. Maar het is meer, het is samenwerken, met elkaar beslissingen nemen en
                het dragen van verantwoordelijkheid. Je leert bij zeilen dingen die ook in het dagelijks leven goed van
                pas komen: respect voor elkaar, samen zorgen voor een goede sfeer en op elkaar vertrouwen.

                <iframe width="560" height="315" src="https://www.youtube.com/embed/7-hIEe-VsuA" frameborder="0"
                        allowfullscreen style="margin-top: 30px"></iframe>

        </div>

        <div class="grid__item2">
            <h1>Onze cursussen:</h1>

            <h2>Beginnerscursus, niveau 1</h2>
            <p>Bij deze cursus leer je de basis van het zeilen, we beginnen bij nul en vertellen precies wat je moet
                doen. Dit niveau is voor iedereen toegangelijk (wel boven de 8 jaar!). we gebruiken een "16-kwadraat"
                boot. zodat de cursist samen met een instructuur kan samenwerken.</p>
            <h4>Prijs vanaf €500,00</h4>

            <h2>Gevorderdecursus, niveau 2</h2>
            <p>In de gevorderdecurus ga je al meer zelfstandig doen. Je zit op een "Draak" en je leert hoe je snel zelf
                beslissingen maakt. Als je deze cursus hebt doorstaan krijg je aan het eind van je week je zeildiploma.
                Het is voorstandig om eerst de beginnerscursus te doen als je nog helemaal geen zeilervaring hebt.</p>
            <h4>Prijs vanaf €700,00</h4>

            <h2>Wad tochten, niveau 3</h2>
            <p>De wadtochten mag je pas aan deelnemen als je een zeildiploma hebt, Ook moet je hiervoor ouder zijn dan
                18 jaar. Je vaart hier met een "Schouw" en je leert hier op de echte zee te zeilen, ook leer je de
                leiding nemen zodat je een echte kapitein kan worden. Verder leer je bijzondere verrichtingen die
                gevorderen niet kunnen.
            </p>
            <h4>Prijs vanaf €800,00</h4>
        </div>
    </div>
@endsection
