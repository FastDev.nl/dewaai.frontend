Beste,

Er is een mail gestuurd via het contact formulier door {{ $name }}.

---
{{ json_encode($message) }}
---

Met vriendelijke groet,
Zeilschool de Waai
