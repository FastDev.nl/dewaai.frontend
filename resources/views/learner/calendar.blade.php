@extends('learner.layout')

@section('learner-content')
    <h2>Agenda</h2>
    <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Agenda</a></li>
    <li><a data-toggle="tab" href="#menu1">Tabel</a></li>
  </ul>
<div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div id='calendar'></div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <table class="table">
        <thead>
        <tr>
            <th>Soort Cursus</th>
            <th>Naam</th>
            <th>Datum</th>
        </tr>
        </thead>
        <tbody>
      @foreach($courses as $cou)
        <tr>
          <td>{{ $cou->type->name }}</td>
          <td>{{ $cou->name }}</td>
          <td>{{ $cou->date_range }}</td>         
      </tr>
      @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection

@section('page-head')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css">
@endsection
@section('page-scripts')
    <div class="modal fade" id="course-modal" tabindex="-1" role="dialog" aria-labelledby="course-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="course-name">course-name</h4>
                </div>
                <div class="modal-body">
                    Deze cursus is van <span id="course-range">course-range</span> en begint om 13:00 uur.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                events: {!! json_encode($events) !!},
                eventClick: function (event) {
                    $('#course-name').text(event.data.type + " - " + event.data.name);
                    $('#course-range').text(event.data.date_range);
                    $('#course-modal').modal('show');
                }
            });
        });
    </script>
@endsection
