@extends('layout')

@section('page-title', 'Mijn Account -')
@section('page-content')
    <div class="information">
        <div class="col-md-3">
            <h5>
                {{ session('user.name_full') }}
                <small><a href="{{ url('/auth/logout') }}" class="pull-right">Uitloggen</a></small>
            </h5>
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="{{ url('/learner/details') }}">Mijn gegevens</a>
                </li>
                <li class="list-group-item">
                    <a href="{{ url('/learner/calendar') }}">Agenda</a>
                </li>
                <li class="list-group-item">
                    <a href="{{ url('/learner/invoices') }}">Facturen</a>
                </li>
            </ul>
        </div>
        <div class="col-md-9">
            @yield('learner-content')
        </div>
    </div>
@endsection
