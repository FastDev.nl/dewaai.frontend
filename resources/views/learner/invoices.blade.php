@extends('learner.layout')

@section('learner-content')
    <h2>Facturen</h2>
    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>Factuurdatum</th>
            <th>Status</th>
            <th>Bekijken</th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
                <td>#{{ $invoice->id }}</td>
                <td>{{ date('d-m-Y', $invoice->created_at->getTimeStamp()) }}</td>
                <td>{{ $invoice->status_string }}</td>
                <td><a href="{{ $invoice->url }}" target="_blank">Bekijken</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
